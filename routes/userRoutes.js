const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers")
const auth = require("./../auth")

router.post("/email-exists", (req, res) => {

	userController.checkEmail(req.body).then( result => res.send(result))
})

//register a user
// http://localhost:3000/api/users
router.post("/register", (req, res) => {

	userController.register(req.body).then( result => res.send(result))
})

router.get("/", (req, res) => {

	userController.getAllUsers().then( result => res.send(result))
})


router.post("/login", (req, res) => {

	userController.login(req.body).then(result => res.send(result))
})

//retrieve user information
router.post("/details", auth.verify, (req, res) => {
	
	let userData = auth.decode(req.headers.authorization)
	// console.log(userData)

	userController.getProfile(userData).then(result => res.send(result))
})


router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
    userController.enroll(data).then(result => res.send(result))
})

router.delete("/delete-user", auth.verify, (req, res) => {
	userController.delete(req.body).then(result => res.send(result))
})

module.exports = router